

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class BanqueUtilisateurs {

    private static BanqueUtilisateurs ListeUsers = null;
    private ArrayList<Utilisateur> listeUtilisateurs;

    //Constructeur de la Banque privé en raison du singleton
    private BanqueUtilisateurs() throws FileNotFoundException {
        listeUtilisateurs = new ArrayList<Utilisateur>(100);

    }

    //Remplie la liste au démarrage
    public void chargerUtilisateur() throws FileNotFoundException {
        Scanner banqueUin = new Scanner(new FileReader("dat/BanqueUtilisateur.txt"));
        while (banqueUin.hasNextLine()) {
            String s = banqueUin.nextLine();
            String[] ss = s.split(";");
            this.addUtilisateur(ss[0], ss[1], ss[2], ss[3], ss[4]);
        }
    }

    //Il s'agit d'un singleton, cette méthode retourne la seule
    //instance de la classe
    public static BanqueUtilisateurs getUserInstance() throws FileNotFoundException {
        if (ListeUsers != null) {
            return ListeUsers;
        } else {
            ListeUsers = new BanqueUtilisateurs();
            ListeUsers.chargerUtilisateur();
            return ListeUsers;
        }


    }

    //Retourne la liste des clients ou des techniciens
    public ArrayList getListUtilisateurs(String role) {
        ArrayList<Utilisateur> users = new ArrayList<Utilisateur>(100);
        for (int i = 0; i < listeUtilisateurs.size(); i++) {
            if (listeUtilisateurs.get(i).getRole().equals(role)) {
                users.add(listeUtilisateurs.get(i));
            }
        }
        return users;
    }

    //Retourne l'utilisateur s'il existe, sinon, retourne null
    public Utilisateur chercherUtilisateur(Utilisateur wanted) {
        if (listeUtilisateurs.contains(wanted)) {
            return listeUtilisateurs.get(listeUtilisateurs.indexOf(wanted));
        } else {
            return null;
        }
    }

    //Trouve l'utilisateur à l'aide de son nom d'utilisateur
    //On assume qu'il n'y a qu'un Utilisateur par nom d'utilisateur
    public Utilisateur chercherNomRole(String nomVoulu) {
        for (int i = 0; i < listeUtilisateurs.size(); i++) {
            if (listeUtilisateurs.get(i).getNom().equalsIgnoreCase(nomVoulu)) {
                return listeUtilisateurs.get(i);
            }

        }
        return null;
    }

    //Ajoute un utilisateur à la liste
    //Il n'y a pas de methode qui enregistre les utilisateurs à chaque fermeture
    //en raison du fait que la liste d'utilisateur ne change jamais.
    public void addUtilisateur(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        if (role.equals("client")) {
            Client client = new Client(prenom, nom, nomUtilisateur, mdp, role);
            listeUtilisateurs.add(client);
        } else {
            Technicien tech = new Technicien(prenom, nom, nomUtilisateur, mdp, role);
            listeUtilisateurs.add(tech);
        }
    }
}